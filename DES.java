public class EncrypDES {
	// KeyGenerator 提供对称密钥生成器的功能，支持各种算法
	private KeyGenerator keygen;
	// SecretKey 负责保存对称密钥
	private SecretKey deskey;
	// Cipher负责完成加密或解密工作
	private Cipher c;
	// 该字节数组负责保存加密的结果
	private String cipherStr;
	// 单例模式
	private static EncrypDES instance = null;

	public static EncrypDES getInstance() {
		if (instance == null) { // line 12
			try {
				instance = new EncrypDES();
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchPaddingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} // line 13
		}
		return instance;
	}

	public EncrypDES() throws NoSuchAlgorithmException, NoSuchPaddingException {
		// 实例化支持DES算法的密钥生成器(算法名称命名需按规定，否则抛出异常)
		keygen = KeyGenerator.getInstance("DES");
		// 生成密钥
		deskey = keygen.generateKey();
		// 生成Cipher对象,指定其支持的DES算法
		c = Cipher.getInstance("DES");
	}

	/**
	 * 对字符串加密
	 * 
	 * @param str
	 * @return
	 * @throws InvalidKeyException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	public String Encrytor(String str) throws InvalidKeyException,
			IllegalBlockSizeException, BadPaddingException {
		// 根据密钥，对Cipher对象进行初始化，ENCRYPT_MODE表示加密模式
		c.init(Cipher.ENCRYPT_MODE, deskey);
		byte[] src = str.getBytes();
		System.out.println("Encrytor------->" + new String(src));
		// 加密，结果保存进cipherByte
		byte[] cipherByte = c.doFinal(src);
		System.out.println("Encrytor2------->" + new String(cipherByte));
		cipherStr = Base64.encodeToString(cipherByte, Base64.DEFAULT);
		System.out.println("cipherStr------->" + cipherStr);
		return cipherStr;
	}

	/**
	 * 对字符串解密
	 * 
	 * @param buff
	 * @return
	 * @throws InvalidKeyException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	public String Decryptor(String str) throws InvalidKeyException,
			IllegalBlockSizeException, BadPaddingException {
		// 根据密钥，对Cipher对象进行初始化，DECRYPT_MODE表示加密模式
		c.init(Cipher.DECRYPT_MODE, deskey);
		byte[] buff = Base64.decode(str, Base64.DEFAULT);
		byte[] cipherByte = c.doFinal(buff);
		cipherStr = new String(cipherByte);
		return cipherStr;
	}